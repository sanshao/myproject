'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
    async index() {
        
        // this.ctx.body = 'hi, egg';
        console.log('this.ctx', this.ctx.req.protocol)
        this.ctx.redirect('/index.html');
        
    }
}

module.exports = HomeController;
