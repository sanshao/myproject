'use strict';

module.exports = appInfo => {
    const config = exports = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_1523724641559_3399';

    // add your config here
    config.middleware = [];

    config.static = {
        maxAge: 0,
        cacheControl: 'no-cache',
        prefix: '/',
        preload: false,
        // dynamic: false // 这个不知道什么鬼 为false时就404了
    };


    return config;
};
